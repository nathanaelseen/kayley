; In x86 (real mode), the GPR (General Purpose Registers) are:
; ax, bx, cx, dx, which are 16 bit registers (2 bytes)
; Also, we're able to set the high/low bytes of these registers
; independently

; Set high byte of ax register to indicate teletype mode
; (for the cursor to advance after placing a character)
mov ah, 0x0e

; Set low byte of ax register to the ASCII code character
; we would like to print
mov al, 'H'

; Raise the interrupt associated to screen printing
; (which our bios would take care for us)
int 0x10

; Repeat for the remaining characters
mov al, 'e'
int 0x10

mov al, 'l'
int 0x10

mov al, 'l'
int 0x10

mov al, 'o'
int 0x10

mov al, ' '
int 0x10

mov al, 'W'
int 0x10

mov al, 'o'
int 0x10

mov al, 'r'
int 0x10

mov al, 'l'
int 0x10

mov al, 'd'
int 0x10

mov al, '!'
int 0x10

; Finally, infinite jumping to its own!
jmp $

; Ask the assembler to pad the binary file with zeroes until
; the 510th byte (including)
times 510-($-$$) db 0

; For the 511th and 512th byte, insert the magic number, 0xAA55
; so the BIOS know that this disk block contains bootable code
; (boot sector)
dw 0xAA55
